# Create your views here.
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse
import os
import tempfile
from django.utils import simplejson
from utils import *
def home(request):
	return render(request,"index.html")

def assembly(request):
	objdump=""
	objdumerr=""
	error=""
	maxsize=1024
	data = request.POST['assembly']
#	print request.POST
	f = tempfile.NamedTemporaryFile(suffix='.s')
#	print f.name
	f.write("""	.text
	.global	testFun
	.type	testFun, %function
testFun:
""")
	if len(data)>maxsize:
		error = error+"Maxsize:"+str(maxsize)+"\n"
		data = cleandata(maxsize,"\n",data)
	f.write(data)
	f.flush()



	(stdin,compilingoutput,compileerr) = os.popen3("arm-none-eabi-gcc -c "+f.name+" -o "+f.name[:-1]+"o")
	compilingoutput =compilingoutput.read()
	compileerr = compileerr.read()
#	print compileerr
	compileerr = "<br>".join([x for x in compileerr.split("\n") if "Error:" in x])
	if os.path.exists(f.name[:-1]+"o"):
		(stdin,objdump,objdumerr) = os.popen3("arm-none-eabi-objdump -d "+f.name[:-1]+"o")
		objdump = objdump.read()
		objdumerr = objdumerr.read()
		objdump = "\n".join(objdump.split("\n")[7:])
	rep={"compiler":compilingoutput,"compilererr":compileerr,"dump":objdump,"dumperr":objdumerr,"error":error}
	f.close()
	os.system("rm "+f.name[:-1]+"o")
#	print rep
	return HttpResponse(simplejson.dumps(rep))
